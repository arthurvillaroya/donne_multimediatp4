#include <stdio.h>
#include "image_ppm.h"
#include <cmath>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, SB, SH;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&SB);
   sscanf (argv[4],"%d",&SH);
   

   OCTET *ImgIn, *ImgOut, *ImgOut2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgOut2, OCTET, nTaille);

 for (int i=0; i < nH; i++){
   for (int j=0; j < nW ; j++){
      if(ImgIn[i*nW+j] <= SB){ImgOut[i*nW+j] = 0;}
      else if(ImgIn[i*nW+j] >= SH){ImgOut[i*nW+j] = 255;}
      else{ImgOut[i*nW+j] = ImgIn[i*nW+j];}
    }
  }
 

 for (int i=1; i < nH - 1; i++){
   for (int j=1; j < nW -1 ; j++){
    if(ImgOut[i*nW+j] <= SH && ImgOut[i*nW+j] >= SB){
      bool voisinB = false;
        for(int l = -1; l <= 1; l++){
          for(int k = -1; k <= 1; k++){
            if(ImgOut[(i+l)*nW+(j+k)] == 255){voisinB = true;}
          }
        if(voisinB){ImgOut2[i*nW+j] = 255;}
        else{ImgOut2[i*nW+j] = 0;}
        }
      }
    else{
        ImgOut2[i*nW+j] = ImgOut[i*nW+j]; 
      }
    }
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut2,  nH, nW);
   free(ImgIn); free(ImgOut2);

   return 1;
}
