#include <stdio.h>
#include "image_ppm.h"
#include <cmath>
#include <stdlib.h>
#include "math.h"
#include <vector>
#include <climits>
#include <iostream>
#include <algorithm>

using namespace std;

void Ppasse(OCTET* ImgIn, int* Label, vector<vector<int>> &Equi, int nW, int nH, int nTaille);
void Spasse(OCTET* ImgIn, int* Label, vector<vector<int>> &Equi, int nW, int nH);
void initLabel(int* Label, int nTaille);
int voisin(int* ImgIn, int i, int j, int nW, int nH);
bool plusieursVoisin(int* ImgIn, int i, int j, int nW, int nH);
int petitVoisin(int* ImgIn, int i, int j, int nW, int nH);
void listVoisin(int* ImgIn, vector<vector<int>> &Equi, int i, int j, int nW, int nH);
bool exist(vector<int> &vec, int a);
void unionVec(vector<vector<int>> &Equi, int mini, vector<int> &list);
int nbLabel(int* Label, int nTaille);
bool elemComun(vector<int> &a, vector<int> &b);
void printVec(vector<int> a);

int main(int argc, char* argv[])
{
 char cNomImgLue[250], cNomImgEcrite[250];
 int nH, nW, nTaille;
 
 if (argc != 3) {
    printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
    exit (1) ;
  }
   
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
   

  OCTET *ImgIn, *ImgOut;
  int* Label;
  vector<vector<int>> LabelsEqui;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);

  nTaille = nH * nW;

   
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille);
  allocation_tableau(Label, int, nTaille);

  Ppasse(ImgIn, Label, LabelsEqui, nW, nH, nTaille);
  printf("%d %d \n", Label[215*nW+81],  Label[260*nW+238]);
  Spasse(ImgIn,Label, LabelsEqui, nW, nH);
  printf("%d %d \n", Label[215*nW+81],  Label[260*nW+238]);
  printVec(LabelsEqui[0]);
   printVec(LabelsEqui[1]);
  int labelDif = nbLabel(Label,nTaille);
  printf("%d \n", labelDif);
  
  for(int i = 0; i<nTaille; i++){
    if(Label[i] != -1){
    ImgOut[i] = ((Label[i]+1) * (255.0 / (labelDif-1)));
    }
    else{ImgOut[i] = 0;}
  }
  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn); free(ImgOut); free(Label);

  return 1;
}

void Ppasse(OCTET* ImgIn, int* Label, vector<vector<int>> &Equi, int nW, int nH, int nTaille){
  initLabel(Label, nTaille);
  int labelC = 1; 
  for(int i = 0; i < nH; i++){
    for(int j = 0; j < nW; j++){
      if(ImgIn[i*nW+j] == 0){
        if(voisin(Label, i, j, nW, nH)){
          Label[i*nW+j] = voisin(Label, i, j, nW, nH);
          if(plusieursVoisin(Label, i, j, nW, nH)){
            listVoisin(Label, Equi, i, j, nW, nH);      
          }
        }
        else{
          Label[i*nW+j] = labelC;
          vector<int> v (1,labelC);
          Equi.push_back(v);
          labelC++;
        } 
      }
    }
  }
  vector<vector<int>> Tab(Equi.size());
  for(int i = 0; i<Equi.size(); i++){
    for(int j = i+1; j<Equi.size(); j++){
      if(elemComun(Equi[i],Equi[j])){
        vector<int> v;
        sort(Equi[i].begin(), Equi[i].end());
        sort(Equi[j].begin(), Equi[j].end());
        set_union(Equi[i].begin(), Equi[i].end(),Equi[j].begin(), Equi[j].end(), back_inserter(v));
        Equi[i] = v;
        Equi[j] = v;
      }
    }
  }
}

void Spasse(OCTET* ImgIn, int* Label, vector<vector<int>> &Equi, int nW, int nH){
 for(int i = 0; i < nH; i++){
    for(int j = 0; j < nW; j++){
      if(ImgIn[i*nW+j] == 0){
        bool lab = false;
        int a = 0;
        while(!lab && a<Label[i*nW+j]){
          lab = exist(Equi[a], Label[i*nW+j]);
          a++;
        }
        Label[i*nW+j] = a;
      }
    }
  }
}

void initLabel(int* Label, int nTaille){
  for(int i=0; i<nTaille; i++){
    Label[i] = -1;
  }
}

int voisin(int* ImgIn, int i, int j, int nW, int nH){
  if(j != 0){
    if(ImgIn[i*nW+(j-1)] != -1){
      return ImgIn[i*nW+(j-1)];
    }
  }
  for(int k = -1; k <= 1; k++){
    if((j+k >= 0) && (j+k < nW) && (i-1 >= 0)){
      if((ImgIn[(i-1)*nW+(j+k)] != -1) ){
        return ImgIn[(i-1)*nW+(j+k)];
      }
    }
  }
  return 0;
}

bool plusieursVoisin(int* ImgIn, int i, int j, int nW, int nH){
  int cpt = 0;
  vector<int> a;
  for(int k = -1; k <= 1; k++){
    if((j+k >= 0) && (j+k < nW) && (i-1 >= 0)){
      if((ImgIn[(i-1)*nW+(j+k)] != -1) && !(exist(a, ImgIn[(i-1)*nW+(j+k)]))){
        cpt++;
        a.push_back(ImgIn[(i-1)*nW+(j+k)]);
      }
    }
  }
  if(j != 0){
    if((ImgIn[i*nW+(j-1)] != -1) && !(exist(a, ImgIn[i*nW+(j-1)]))){
      cpt++;
      a.push_back(ImgIn[i*nW+(j-1)]);
    }
  }
  return cpt > 1;
}

int petitVoisin(vector<int> list){
  int mini = list[0];
  for(int i = 1; i < list.size(); i++){
    mini = min(mini, list[i]);
  }
  return mini;  
}

void listVoisin(int* ImgIn, vector<vector<int>> &Equi, int i, int j, int nW, int nH){
  vector<int> list;
  for(int k = -1; k <= 1; k++){
    if((j+k >= 0) && (j+k < nW) && (i-1 >= 0)){
      if((ImgIn[(i-1)*nW+(j+k)] != -1) && !(exist(list, ImgIn[(i-1)*nW+(j+k)]))){
        list.push_back(ImgIn[(i-1)*nW+(j+k)]);
      }
    }
  }
  if(j != 0){
    if((ImgIn[i*nW+(j-1)] != -1) && !(exist(list, ImgIn[i*nW+(j-1)]))){
      list.push_back(ImgIn[i*nW+(j-1)]);
    }
  }
  int mini = petitVoisin(list);
  unionVec(Equi,mini,list);
}

bool exist(vector<int> &vec, int a){
  return (find(vec.begin(), vec.end(), a) != vec.end());
}

void unionVec(vector<vector<int>> &Equi, int mini, vector<int> &list){
  for(int c = 0; c < list.size(); c++){
    vector<int> v;
    sort(Equi[mini].begin(), Equi[mini].end());
    sort(Equi[list[c]-1].begin(), Equi[list[c]-1].end());
    set_union(Equi[mini].begin(), Equi[mini].end(),Equi[list[c]-1].begin(), Equi[list[c]-1].end(), back_inserter(v));
    Equi[mini] = v;
    Equi[list[c]-1] = v;
  }
}

int nbLabel(int* Label, int nTaille){
  int count = 0;
  vector<int> vec;
  for(int i = 0; i < nTaille; i++){
    if (!exist(vec,Label[i]) && Label[i] != -1){
      count++;
      vec.push_back(Label[i]);
    }
  }

  return count;
}

bool elemComun(vector<int> &a, vector<int> &b){
  for(int i = 0; i<a.size(); i++){
    if(exist(b,a[i])){return true;}
  }
  return false;
}

void printVec(vector<int> a){
  for(int i = 0; i<a.size(); i++){
    printf("%d %s",a[i], "|");
  }
  printf("\n");
}
