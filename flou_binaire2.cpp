// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"
#include <stdlib.h>

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgLue2[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageInBianre.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2);
   sscanf (argv[3],"%s",cNomImgEcrite);
   

   OCTET *ImgIn, *ImgInBinaire, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgInBinaire, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgInBinaire, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

 for (int i=1; i < nH - 1; i++)
   for (int j=1; j < nW - 1; j++)
     {
      int som = 0;
      int somCoef = 0;
      if(ImgInBinaire[i*nW+j] == 255){
        for(int x = -1; x <= 1; x++){
          for(int y = -1; y <= 1; y++){
            som += ImgIn[(i+x)*nW+(j+y)] * ImgInBinaire[(i+x)*nW+(j+y)];
            somCoef += ImgInBinaire[(i+x)*nW+(j+y)];
          }
        }
        ImgOut[i*nW+j] = som/somCoef;
      }
      else{
        ImgOut[i*nW+j] = ImgIn[i*nW+j];
      }
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

  return 1;
}
